# Organisation Application DB Assignment

## Task
Design database for the following application using https://dbdiagram.io/


### Application Overview:
An application for a company to manage employee information and daily progress. 

### Actors:

There are 3 actors for the application:

* Administrator
* Employee
* Reporting Managers

### Use Cases: 

#### For administrator:

* User can login into the system using organisation email and password
* User can view all the employee in the organisation
* User can add a new employee to the organisation, details of employee include:
    * Name
    * Email
    * Designation
    * Password
    * Phone number
    * Blood group
    * Permanent Address
    * Current Address
    * Father's Name
    * Mother's Name
* User can edit employee information 
* User can delete an employee from the organisation
* An employee can be a manager if user is assigned as reporting manager of the employee
* User can assign a reporting manager to the employee

#### For Employee:
* User can login into the application from the email and the password provided by the admin
* User can view his detail in the organisation
* User can add daily update using the application, for daily updates fileds will be:
    * Date
    * In-Time
    * Out-Time
    * Summary of task

#### For Manager:
* All the features of Employee, manager can also add daily update
* User can view all the employees reporting the the manager
* User can view the daily updates of employees who are reporting to the user

## Database should satisfy the following assumptions:

* All the users in the application (Admin, Manager and Employee) are employee of the organisation. In another words we can say that amdinistrator and managers are subset of employee.
* An employee can be a manager. Or an employee can be an administrator.
* An employee can be a reporting manager of another employee. But only one employee will be the reporting manager of an employee. There will be no multiple reporting managers.






